.. index::
   single: Monte Carlo; Calculators

.. module:: mchammer.calculators

Calculators
===========


.. _cluster_expansion_calculator:

.. index::
   single: Function reference; ClusterExpansionCalculator
   single: Class reference; ClusterExpansionCalculator
   single: Monte Carlo; Cluster expansion calculator

ClusterExpansionCalculator
--------------------------

.. autoclass:: ClusterExpansionCalculator
   :members:
   :undoc-members:
   :inherited-members:

.. _target_vector_calculator:

.. index::
   single: Function reference; TargetVectorCalculator
   single: Class reference; TargetVectorCalculator
   single: Monte Carlo; Target vector calculator

TargetVectorCalculator
--------------------------

.. automodule:: mchammer.calculators.target_vector_calculator
   :members:
   :undoc-members:
   :inherited-members: