.. index::
   single: Function reference; Python core components
   single: Class reference; Python core components

Additional Python components
============================

BaseOptimizer
-------------

.. automodule:: icet.fitting.base_optimizer
   :members:
   :undoc-members:
   :inherited-members:

Cluster
-------

.. automodule:: icet.core.cluster
   :members:
   :undoc-members:
   :inherited-members:

ClusterCounts
-------------

.. automodule:: icet.core.cluster_counts
   :members:
   :undoc-members:
   :inherited-members:

Geometry
--------

.. automodule:: icet.tools.geometry
   :members:
   :undoc-members:
   :inherited-members:

LatticeSite
-----------

.. automodule:: icet.core.lattice_site
   :members:
   :undoc-members:

LocalOrbitListGenerator
-----------------------

.. automodule:: icet.core.local_orbit_list_generator
   :members:
   :undoc-members:

ManyBodyNeighborList
--------------------

.. automodule:: icet.core.many_body_neighbor_list
   :members:
   :undoc-members:
   :inherited-members:

NeighborList
------------

.. automodule:: icet.core.neighbor_list
   :members:
   :undoc-members:
   :inherited-members:

Orbit
-----

.. automodule:: icet.core.orbit
   :members:
   :undoc-members:
   :inherited-members:

OrbitList
---------

.. automodule:: icet.core.orbit_list
   :members:
   :undoc-members:
   :inherited-members:

PermutationMatrix
-----------------

.. automodule:: icet.core.permutation_matrix
   :members:
   :undoc-members:
   :inherited-members:

Structure
---------

.. automodule:: icet.core.structure
   :members:
   :undoc-members:
   :inherited-members:
