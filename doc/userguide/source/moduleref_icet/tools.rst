.. index::
   single: Function reference; Tools
   single: Class reference; Tools

Tools
=====

.. index::
   single: Function reference; Convex hull
   single: Class reference; Convex hull

Convex hull construction
------------------------

.. automodule:: icet.tools
   :members: ConvexHull


.. index::
   single: Function reference; Structure mapping

Mapping structures
------------------

.. autofunction:: icet.tools.map_structure_to_reference


.. _structure_enumeration:

.. index::
   single: Function reference; Structure enumeration
   single: Class reference; Structure enumeration

Structure enumeration
---------------------

.. autofunction:: icet.tools.enumerate_structures
.. autofunction:: icet.tools.enumerate_supercells


.. index::
   single: Function reference; SQS generation
   single: Class reference; SQS generation
   single: Function reference; Special structures
   single: Class reference; Special structures

Generation of special structures
--------------------------------

.. automodule:: icet.tools.structure_generation
   :members:
   :undoc-members:
   :inherited-members:


.. index::
   single: Function reference; Wyckoff sites
   single: Class reference; Wyckoff sites
   single: Function reference; Other structure tools
   single: Class reference; Other structure tools

Other structure tools
---------------------

.. autofunction:: icet.tools.get_primitive_structure
.. autofunction:: icet.tools.get_wyckoff_sites
